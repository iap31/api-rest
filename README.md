# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Deployment** para conocer cómo desplegar el proyecto.


### Pre-requisitos 📋


_El primer paso a realizar es la instalación de NodeJS. Para ello, haremos uso de los siguientes comandos en la terminal_

```
sudo apt install npm
```

_A continuación, realizaremos la instalacion de n mediante npm para ayudarnos a instalar y mantener las versiones de Node_

```
sudo npm clean -f
sudo npm i -g n
```
_Para terminar este oaso, instalaremos la última versión de Node mediante n_

```
sudo n stable
```
_Instalar la biblioteca Express la cual facilita la gestión de métodos y recursos HTTP_

```
nmp i -S express
```

_Instalación de Postman para hacer invocaciones al servidor_

```
https://www.getpostman.com/
```

_Instalación del gestor de proyectos Nodemon a través de los siguientes comandos_

```
cd
cd node
cd api-rest

npm i -D nodemon
```

_En la sección de scripts del archivo package.json añadir la linea_

```
"Start": "nodemon index.js",
```
_Ahora podremos inicar el proyecto de forma que cada vez que realicemos un cambio este se actualizará automaticamente mediante el comando_

```
npm start
```
_Ahora deberemos intalar el motor de registro Morgan mediante el comando_

```
npm i -S morgan
```
_Instalar MongoDB para trabajar con una base de datos no estructurada_

```
sudo apt update
sudo apt install -y mogodb
```
_Finalmente instalaremos la biblioteca mongodb y la biblioteca mongojs la cual nos simplificará el acceso a mongo desde nuestro proyecto node_

```
npm i -S mongodb
npm i -S mongojs
```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Indica cómo será ese paso_

```
Proporciona un ejemplo
```

_Y repite_

```
hasta finalizar
```

_Finaliza con un ejemplo de cómo obtener datos del sistema o cómo usarlos para una pequeña demo_

## Ejecutando las pruebas ⚙️

_Primeramente ponemos el servicio en marcha a traves del comando_

```
npm start
```

_Una vez hecho esto, a través de Postman iremos realizando peeticiones para probar nuestro servicio_

_Petición POST_

```
POST http://localhost:3000/api/familia

Cuerpo (raw , JSON)
    {
        "tipo": "Hermano",
        "nombre": "Pepe",
        "edad": 46
    }
```
_Petición GET_

```
GET http://localhost:3000/api
```
_Petición GET_

```
GET http://localhost:3000/api/familia
```

_Petición GET_

```
GET http://localhost:3000/api/mascotas
```

_Petición PUT_

```
PUT http://localhost:3000/api/mascotas/5ad6551ff6efc76d03ddbe70
Body (raw, JSON)
{
    "color": "marrón",
    "nombre": "Nica"
}
```


## Despliegue 📦

_Realizar un git clone del siguiente enlace_

```
git clone https://iap31@bitbucket.org/iap31/api-rest.git
```

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [ROME](https://rometools.github.io/rome/) - Usado para generar RSS

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/tuProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Paco Maciá** - *Trabajo Inicial* - [pmacia](https://github.com/pmacia)
* **Fulanito Detal** - *Documentación* - [fulanitodetal](#fulanito-de-tal)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quiénes han participado en este proyecto. 

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.
