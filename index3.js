'use strict'

const express = require('express');
const app = express();

app.get('/Hola', (request, response) => {
    response.send('Hola a todas y todos desde Express!')
});

app.listen(8080, () => {
    console.log('API REST ejecutandose en http://localhost:8080/hola');
});