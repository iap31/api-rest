var http = require('http');
var server = http.createServer();

function HTTP_Response(request, response) {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Hola a todas y todes!\n');
}

server.on('request', HTTP_Response);
server.listen(8080);
console.log('Servidor ejecutandose en puerto 8080...');